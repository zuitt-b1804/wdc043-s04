public class Main {
//    public String sample = "Sample";
    public static void main(String[] args) {
//        Main newSample=new Main();
//        System.out.println(newSample.sample);
        Car car1 = new Car();
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
//
//        car1.make="Veyron";
//        car1.brand="Bugatti";
//        car1.price=2000000;
//        System.out.println(car1.brand);
//        System.out.println(car1.make);
//        System.out.println(car1.price);
        // Each instance of a class should be independent from one another, especially their properties.

        Car car2 = new Car();
//        car2.make="Tamaraw FX";
//        car2.brand="Toyota";
//        car2.price=450000;
//
//        System.out.println(car2.brand);
//        System.out.println(car2.make);
//        System.out.println(car2.price);

        //Mini Activity
        /*
        Create a new instance of the Car class and save it in a variable called car3.
        Access the properties of the instance and update its values.
         */

        Car car3 = new Car();
//        car3.make="Montero";
//        car3.brand="Mitsubishi";
//        car3.price=1450000;
//
//        System.out.println(car3.make);
//        System.out.println(car3.brand);
//        System.out.println(car3.price);
        car3.start();
        car1.start();
        car2.start();

        car1.setMake("Veyron");
        System.out.println(car1.getMake());

        car1.setBrand("Bugatti");
        System.out.println(car1.getBrand());

        car1.setPrice(2000000);
        System.out.println(car1.getPrice());

        //when creating a new instance of a class:
        //new allows us to create a new instance.
        //Car()-constructor of our class - without arguments - default constructor-which Java can define for us.
        Car car4= new Car();
//        System.out.println(car4.getMake());



        Driver driver1 =new Driver("Karlo",26,"Cainta,Rizal");
        System.out.println("Driver1 details are: ");
        System.out.println(driver1.getName());
        System.out.println(driver1.getAge());
        System.out.println(driver1.getAddress());

        Car car5= new Car("Corolla","Toyota",500000,driver1);
        System.out.println(car5.getMake());
        System.out.println(car5.getBrand());
        System.out.println(car5.getPrice());
        System.out.println(car5.getCarDriverName());

        Animal animal1 = new Animal("Dog","Black");
        animal1.call();

    }




}