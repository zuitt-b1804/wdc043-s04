public class Car {

    //Attributes and Methods

    //An object is an idea of a real world object
    //A class is the code that describes the object
    //An instance is a tangible copy of an idea instantiated or created from a class

    //public - the variable in the class is accessible anywhere in application.
    //Class attributes/properties must not be public, they should only be accessed and set via class methods called setters and getters.
    //private - limit the access and ability to set a variable/method to its class.
    //setters are public methods which will allow us to set the attribute of an instance.
    //getters are public methods which will allow us to set the attribute of an instance.
    private String make;
    private String brand;
    private int price;
    //Add a driver variable
    private Driver carDriver;

    //methods are functions of an object which allows us to perform certain tasks
    //void - means that the function does not return anything. Because in Java, functions/methods' return dataType must be declared.
    public void start(){
        System.out.println("Vroom! Vroom!");
    }

    //parameters in Java needs its dataType declared
/*
    public void setMake(String makeParams){
        //"this" keyword refers to the object where the constructor or setter is.
        this.make=makeParams;
    }

    public String getMake(){
        return this.make;
    }
*/

    /*
    Mini - activity
        create setters and getters for the brand and price property of our object
     */
/*    public void setBrand(String brandParams){
        //"this" keyword refers to the object where the constructor or setter is.
        this.brand=brandParams;
    }

    public String getBrand(){
        return this.brand;
    }

    public void setPrice(Integer priceParams){
        //"this" keyword refers to the object where the constructor or setter is.
        this.price=priceParams;
    }

    public Integer getPrice(){
        return this.price;
    }*/

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    //constructor is a method which allows to set the initial values of an instance.
    //empty/default constructor - allows us to create an instance with default initialized values for our properties.
    public Car(){
        //empty or you can designate default values instead of getting them from parameters.
        //Java, actually already creates one for us, however, empty/default constructor made just by Java allows Java to set its own default values. If you create your own, you can set your own default values.
    }

    //parameterized constructor - allows us to initialize values to our attributes upon creation of the instance.
//    public Car(String make,String brand,int price){
//        this.make=make;
//        this.brand=brand;
//        this.price=price;
//    }


    public Car(String make, String brand, int price, Driver driver) {
        this.make = make;
        this.brand = brand;
        this.price = price;
        this.carDriver=driver;
    }

    public Driver getCarDriver() {
        return carDriver;
    }

    public void setCarDriver(Driver carDriver) {
        this.carDriver = carDriver;
    }

    public String getCarDriverName(){
        return this.carDriver.getName();
    }
}
